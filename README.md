## Jurata Assignment

First, make sure that you have node and npm installed in your machine, if you don't you cand find in this link [https://nodejs.org/en/](https://nodejs.org/en/):

Then Install the packages

```bash
npm install
# or
yarn
```

Now, start your application:

Then Install the packages

```bash
npm run dev
# or
yarn dev
```

Now open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

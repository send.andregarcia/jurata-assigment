const api = async (url, method, data = {}) => {
  const res = await fetch(url, {
    body: JSON.stringify(data),
    method: method,
    headers: {
      Authorization: 'Bearer MGMxMjRjZmEtYTQ3ZC00ZDE2LWIxZjctYjIyZTIxMjA1Yzdj',
      'Content-Type': 'application/json',
    },
  })

  return await res.json()
}

export default api

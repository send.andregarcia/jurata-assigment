import React from 'react'
import { NextPage } from 'next'
import Error from 'next/error'
import { LocaleProvider } from '../context/LocaleContext'
import { isLocale } from '../translations/getInitLocale'

export default (WrappedPage) => {
  const WithLocale = ({ locale, ...pageProps }) => {
    if (!locale) {
      return <Error statusCode={404} />
    }
    return (
      <LocaleProvider lang={locale}>
        <WrappedPage {...pageProps} />
      </LocaleProvider>
    )
  }

  WithLocale.getInitialProps = async (ctx) => {
    let pageProps = {}
    if (WrappedPage.getInitialProps) {
      pageProps = await WrappedPage.getInitialProps(ctx)
    }

    if (typeof ctx.query.lang !== 'string' || !isLocale(ctx.query.lang)) {
      return { ...pageProps, locale: undefined }
    }

    return { ...pageProps, locale: ctx.query.lang }
  }

  return WithLocale
}

import { gql, useQuery } from '@apollo/client'

export const ALL_ANSWERS_QUERY = gql`
  query allAnswers {
    queryAnswers {
      answer
      id
      image
      question
      url
    }
  }
`

export const FIND_ANSWER_QUESTION = gql`
  query allAnswers($search: String) {
    queryAnswers(filter: { question: { alloftext: $search } }) {
      answer
      id
      image
      question
      url
      search
    }
  }
`

export const FILTER_ANSWERS_SEARCH = gql`
  query allAnswers($search: String) {
    queryAnswers(filter: { search: { regexp: $search } }) {
      answer
      id
      image
      question
      url
      search
    }
  }
`

import { Col, Layout, Menu, Row } from 'antd'
import MainHeader from '../../components/header'
import SearchInput from '../../components/input'
import withTranslations from '../../hocs/withTranslations'
import styles from '../../styles/Home.module.css'

const { Content } = Layout

function Home(props) {
  return (
    <Layout className={styles.container}>
      <MainHeader />
      <Content>
        <Col>
          <Row justify="center" align="top">
            <Col xs={20} md={12}>
              <SearchInput options={[]}></SearchInput>
            </Col>
          </Row>
        </Col>
      </Content>
    </Layout>
  )
}

export default withTranslations(Home)

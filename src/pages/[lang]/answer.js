import { Col, Layout, Row } from 'antd'
import gql from 'graphql-tag'
import React, { useEffect } from 'react'
import client from '../../apollo-client'
import {
  ALL_ANSWERS_QUERY,
  FILTER_ANSWERS_SEARCH,
  FIND_ANSWER_QUESTION,
} from '../../apollo/queries'
import CardAswer from '../../components/cardAnswer'
import MainHeader from '../../components/header'
import SearchInput from '../../components/input'
import { LocaleContext, LocaleProvider } from '../../context/LocaleContext'
import api from '../../services/api'
import styles from '../../styles/Home.module.css'

const { Content } = Layout

const Answer = (props) => {
  const { locale } = React.useContext(LocaleContext)
  useEffect(() => {
    const addAnswerToCache = async (answer) => {
      const { data } = await client.query({
        query: FIND_ANSWER_QUESTION,
        variables: {
          search: props.question,
        },
      })

      if (data.queryAnswers.length == 0) {
        await client.mutate({
          mutation: gql`
            mutation addAnswer($answer: AddAnswersInput!) {
              addAnswers(input: [$answer]) {
                answers {
                  id
                  answer
                  image
                  question
                  url
                  search
                }
              }
            }
          `,
          variables: {
            answer: answer,
          },
        })
      }
    }

    addAnswerToCache({
      ...props.res,
      question: props.question,
      search: props.question,
    })
  }, [])

  return (
    <LocaleProvider lang={locale}>
      <Layout className={styles.container}>
        <MainHeader />
        <Content style={{ padding: 10 }}>
          <Col>
            <Row gutter={[16, 16]} justify="center" align="top">
              <Col xs={24} span={12}>
                <SearchInput options={[]}></SearchInput>
              </Col>
              <Col justify="center" align="top">
                <CardAswer
                  className="max-width-1098"
                  question={props.question}
                  res={props.res}
                ></CardAswer>
              </Col>
            </Row>
          </Col>
        </Content>
      </Layout>
    </LocaleProvider>
  )
}

export default Answer

export async function getServerSideProps(context) {
  const { q } = context.query
  const res = await api('http://api.m3o.com/v1/answer/Question', 'POST', {
    query: q,
  })

  return {
    props: { question: q, res, locale: context.query.lang },
  }
}

import React from 'react'
import Flag from 'react-flags'
import { useRouter } from 'next/dist/client/router'
import { locales, languagesNames } from '../translations/config'
import { LocaleContext } from '../context/LocaleContext'
import { Button, Dropdown, Menu, Space } from 'antd'
import { DownOutlined, UserOutlined } from '@ant-design/icons'

const LocaleSwitcher = () => {
  const router = useRouter()
  const { locale } = React.useContext(LocaleContext)

  const handleLocaleChange = React.useCallback(
    (e) => {
      const regex = new RegExp(`^/(${locales.join('|')})`)
      router.push(router.pathname, router.asPath.replace(regex, `/${e.key}`))
    },
    [router],
  )

  const langs = {
    en: (
      <Button>
        <Space>
          <Flag
            style={{ marginRight: 10 }}
            basePath="/flags"
            name="US"
            format="png"
            pngSize={16}
            alt="Canada Flag"
          />
          English <DownOutlined />
        </Space>
      </Button>
    ),
    pt: (
      <Button>
        <Space>
          <Flag
            style={{ marginRight: 10 }}
            basePath="/flags"
            name="PT"
            format="png"
            pngSize={16}
            alt="Canada Flag"
          />
          Português <DownOutlined />
        </Space>
      </Button>
    ),
    de: (
      <Button>
        <Space>
          <Flag
            style={{ marginRight: 10 }}
            basePath="/flags"
            name="DE"
            format="png"
            pngSize={16}
            alt="Canada Flag"
          />
          Deutsch <DownOutlined />
        </Space>
      </Button>
    ),
  }

  const menu = (
    <Menu onClick={handleLocaleChange}>
      <Menu.Item
        key="pt"
        icon={
          <Flag
            basePath="/flags"
            name="PT"
            format="png"
            pngSize={16}
            alt="Canada Flag"
          />
        }
      >
        Português
      </Menu.Item>
      <Menu.Item
        key="de"
        icon={
          <Flag
            basePath="/flags"
            name="DE"
            format="png"
            pngSize={16}
            alt="Canada Flag"
          />
        }
      >
        Deutsch
      </Menu.Item>
      <Menu.Item
        key="en"
        icon={
          <Flag
            basePath="/flags"
            name="US"
            format="png"
            pngSize={16}
            alt="Canada Flag"
          />
        }
      >
        English
      </Menu.Item>
    </Menu>
  )

  return (
    <div style={{ position: 'absolute', top: 5, right: 5 }}>
      <Dropdown overlay={menu}>{langs[locale]}</Dropdown>
    </div>
  )
}

export default LocaleSwitcher

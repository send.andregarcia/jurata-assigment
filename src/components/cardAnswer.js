import { Card } from 'antd'
import useTranslation from '../hooks/useTranslation'

const CardAnswer = (props) => {
  const { locale, t } = useTranslation()

  return (
    <Card title={t('search-result')}>
      <Card
        type="inner"
        title={props.question}
        extra={
          <a target="_blank" href={props.res.url}>
            {t('more')}
          </a>
        }
      >
        {props.res.answer}
      </Card>
    </Card>
  )
}

export default CardAnswer

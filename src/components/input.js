import { AutoComplete, Input } from 'antd'
import { useEffect, useState } from 'react'
import client from '../apollo-client'
import { FILTER_ANSWERS_SEARCH } from '../apollo/queries'
import useTranslation from '../hooks/useTranslation'
import styles from '../styles/SearchInput.module.css'

export default function SearchInput(props) {
  const { locale, t } = useTranslation()
  const [options, setOptions] = useState([])

  const handleOnChange = async (e) => {
    const { data } = await client.query({
      query: FILTER_ANSWERS_SEARCH,
      variables: {
        search: `/.*${e}.*/i`,
      },
    })

    let newOptions = data.queryAnswers.map((answer) => ({
      value: answer.question,
    }))

    setOptions(newOptions)
  }

  return (
    <div className={styles.container}>
      <div className={styles.holder}>
        <form action={`/${locale}/answer`}>
          <AutoComplete
            className={styles.search}
            options={options}
            placeholder={t('input-holder')}
            filterOption={(inputValue, option) =>
              option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !==
              -1
            }
            onChange={handleOnChange}
          >
            <Input.Search name={'q'}></Input.Search>
          </AutoComplete>
        </form>
      </div>
    </div>
  )
}

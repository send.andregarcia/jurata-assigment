import { Layout, Menu } from 'antd'
import styles from '../styles/Header.module.css'
import Image from 'next/image'
import LocaleSwitcher from './languageSwitcher'
import withTranslations from '../hocs/withTranslations'
const { Header } = Layout

function MainHeader(props) {
  return (
    <Header className={styles.header}>
      <Menu
        className={styles.menu}
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['2']}
      >
        <div className="logo">
          <Image src="/logo.svg" alt="Jurata Logo" width={72} height={16} />
        </div>
      </Menu>
      <LocaleSwitcher></LocaleSwitcher>
    </Header>
  )
}

export default MainHeader

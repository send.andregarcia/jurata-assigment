export const locales = ['de', 'en', 'pt']

export const languagesNames = {
  de: 'Deutsch',
  en: 'English',
  pt: 'Português',
}

export const defaultLocale = 'en'

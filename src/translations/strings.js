const strings = {
  en: {
    'input-holder': 'Type your search',
    'search-result': 'Search result',
    more: 'More',
  },
  de: {
    'input-holder': 'Geben Sie Ihre Suche ein',
    'search-result': 'Suchergebnisse',
    more: 'Mehr',
  },
  pt: {
    'input-holder': 'Digite sua pesquisa',
    'search-result': 'Resultado da Pesquisa',
    more: 'Mais',
  },
}

export default strings
